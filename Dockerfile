# 使用官方的 Node.js 18 Alpine 镜像作為基礎鏡像
FROM node:18-alpine AS build

# 設置工作目錄
WORKDIR /app

# 將 package.json 和 package-lock.json（如果有）複製到工作目錄
COPY package*.json ./

# 安装项目依賴
RUN npm install

# 將應用程序的所有文件複製到工作目錄
COPY . .

# 構建應用程序
RUN npm run build

# 使用輕量級的 Nginx 鏡像来提供靜態文件
FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html

# 暴露應用程式運行的端口
EXPOSE 80

# 啟動 Nginx
CMD ["nginx", "-g", "daemon off;"]