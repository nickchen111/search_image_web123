"use strict";
const { describe, it } = require("mocha");
const { expect } = require("chai");

describe("這只是個簡單的測試", () => {
  it("1 + 1 等於多少", () => {
    expect(1 + 1).to.equal(2);
  });

  it("真的假不了！", () => {
    expect(true).to.not.equal(false);
  });
});
